$(function(){
	$('#mapA').on('input', function (event) {
		mapA = this.value;
		console.log('mapA = ', mapA);
	});
	$('#mapB').on('input', function (event) {
		mapB = this.value;
		console.log('mapB = ', mapB);
	});
	$('#mapC').on('input', function (event) {
		mapC = this.value;
		console.log('mapC = ', mapC);
	});
	$('#mapD').on('input', function (event) {
		mapD = this.value;
		console.log('mapD = ', mapD);
	});
});

let graph;
let mapA, mapB, mapC, mapD;

function setup()
{
	mapA = 1, mapB = 0, mapC = 0, mapD = 1;
	let win = createCanvas(700, 700);
	win.id("win")
	graph = new Graph(-5, 5, -4, 4);
};


function draw()
{
	background(color(100,100,100));
	graph.axisCoordinates(0,0,1,1);
	graph.colorGraph = color(100,0,100);
	graph.widthGraph = 4;
	// graph.draw2D((x)=>{return x});
	let points = [-2,-2,  0,2,  2,-2,  1,0,  -1,0]
	points = affinMap(points,
		[
			[mapA, mapB],
			[mapC, mapD]
		]
	);
	graph.draw2D(points);
};

function affinMap (points, map, shift) {
	shift = shift || [0,0];
	let newPoints = [];
	for (let i = 0; i < points.length; i+=2) {
		newPoints[i] = points[i] * map[0][0] + points[i+1] * map[0][1] + shift[0];
		newPoints[i+1] = points[i] * map[1][0] + points[i+1] * map[1][1] + shift[1];
	}
	return newPoints;
};

var Graph = function(a,b,c,d) {
	this.a = a || 0;
	this.b = b || width;
	this.c = c || 0;
	this.d = d || height;

	this.widthPixel = width;
	this.heightPixel = height;

	this.colorGraph = color(0,0,0);
	this.widthGraph = 1;
	this.colorAxis = color(0,0,0);
	this.widthAxis = 1;

	this.mapX = (x) => {
		return (x - this.a) / (this.b - this.a) * this.widthPixel;
	}

	this.mapY = (y) => {
		return this.heightPixel - (y - this.c) / (this.d - this.c) * this.heightPixel;
	}

	this.axisCoordinates = (axisX, axisY, delX, delY) =>{
		stroke(this.colorAxis);
		strokeWeight(this.widthAxis);
		//Oy
		line(this.mapX(axisY), 0, this.mapX(axisY), this.heightPixel);
		//Ox
		line(0, this.mapY(axisX), width, this.mapY(axisX));

		let xCountLine = (this.b - this.a) / delX;
		let yCountLine = (this.d - this.c) / delY;

		let longLine = 5;
		//Штрихы по Ox
		let x = this.a;
		for (let i = 0; i < xCountLine; i++) {
			x += delX;
			line(this.mapX(x), this.mapY(axisX) - longLine, this.mapX(x), this.mapY(axisX) + longLine);
		}
		//Штрихы по Oy
		let y = this.c;
		for (let i = 0; i < yCountLine; i++) {
			y += delY;
			line(this.mapX(axisY) - longLine, this.mapY(y), this.mapX(axisY) + longLine, this.mapY(y));
		}
	}

	this.draw2D = (f) =>
	{
		stroke(this.colorGraph);
		strokeWeight(this.widthGraph);
		switch (typeof f) {
			case 'function': {
				let delPlot = 100;
				let xStep = (this.b - this.a) / delPlot;

				let x = this.a;
				let xLast = this.mapX(x), yLast = this.mapY(f(x));
				for (let i = 0; i < delPlot; i++)
				{
					x += xStep;
					let xNow = this.mapX(x), yNow = this.mapY(f(x));
					line(xLast, yLast, xNow, yNow);
					xLast = xNow;
					yLast = yNow;
				}
				break;
			}
			case 'object':
				if (!Array.isArray(f))
					break;
				let xLast = this.mapX(f[0]), yLast = this.mapY(f[1]);
				for (let i = 2; i < f.length; i+=2) {
					let xNow = this.mapX(f[i]), yNow = this.mapY(f[i+1]);
					line(xLast, yLast, xNow, yNow);
					xLast = xNow;
					yLast = yNow;
				}
			break;
		}
	}
};
